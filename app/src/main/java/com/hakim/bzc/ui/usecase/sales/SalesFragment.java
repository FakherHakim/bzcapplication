package com.hakim.bzc.ui.usecase.sales;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hakim.bzc.R;
import com.hakim.bzc.data.model.entity.Sale;
import com.hakim.bzc.ui.usecase.detail.SaleDetailActivity;
import com.hakim.bzc.ui.usecase.sales.adapter.SalesAdapterListener;
import com.hakim.bzc.ui.usecase.sales.adapter.SalesListAdapter;
import com.hakim.bzc.utils.UtilMethods;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fakher on 13/03/18.
 */

public class SalesFragment extends Fragment implements SalesContract.View {

    private SalesContract.Presenter mPresenter;
    private SalesListAdapter mListAdapter;
    private ProgressBar mProgressBar;
    private RecyclerView mSalesRecyclerView;
    private TextView mNoSalesTextView;

    public static SalesFragment newInstance() {
        return new SalesFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mListAdapter = new SalesListAdapter(new ArrayList<>(0));
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setPresenter(SalesContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_sales, container, false);

        // Set up tasks view
        mSalesRecyclerView = root.findViewById(R.id.sales_fragment_recycler_view);
        mSalesRecyclerView.setAdapter(mListAdapter);

        if (UtilMethods.getScreenOrientation(getContext()) == Configuration.ORIENTATION_PORTRAIT) {
            mSalesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        } else {
            mSalesRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(
                    //If it's tablet, set span to 6, 3 otherwise.
                    getResources().getBoolean(R.bool.isTablet) ? 6 : 3,
                    //Set a vertical scroll for the recycler view.
                    StaggeredGridLayoutManager.VERTICAL));
        }

        mProgressBar = root.findViewById(R.id.loading_detail_progress_bar);
        mNoSalesTextView = root.findViewById(R.id.sales_fragment_no_sales_display);

        setHasOptionsMenu(true);

        return root;
    }

    @Override
    public void setLoadingIndicatorVisible(boolean active) {
        mProgressBar.setVisibility(active ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showSales(List<Sale> sales) {

        mListAdapter.setItems(sales);

        //Item click listener implementation.
        SalesAdapterListener.addTo(mSalesRecyclerView)
                .setOnItemClickListener((recyclerView, position, v) ->
                        mPresenter.openSaleDetail(mListAdapter.getItems().get(position)));

        //Data is available, make recycler view visible.
        mSalesRecyclerView.setVisibility(View.VISIBLE);
        mNoSalesTextView.setVisibility(View.GONE);
    }

    @Override
    public void showSaleDetailsUi(Sale sale) {

        Intent intent = new Intent(getContext(), SaleDetailActivity.class);
        intent.putExtra(SaleDetailActivity.EXTRA_SALE_DETAIL_ID, sale);
        startActivity(intent);
    }

    @Override
    public void showLoadingSalesError(int httpExceptionCode) {
        UtilMethods.showAlertDialog(getActivity(), R.string.network_error_title_dialog,
                //Error code 5XX: Internal server error.
                //Error code 4XX: Client error.
                httpExceptionCode < 499 ?
                        R.string.cannot_reach_server_error : R.string.internal_server_error,
                (dialog, which) -> dialog.dismiss());
    }

    @Override
    public void showNoSales() {
        mSalesRecyclerView.setVisibility(View.GONE);
        mNoSalesTextView.setVisibility(View.VISIBLE);
    }
}
