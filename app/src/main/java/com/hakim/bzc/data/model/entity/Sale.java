package com.hakim.bzc.data.model.entity;

import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Unique;

import java.io.Serializable;

/**
 * Created by fakher on 12/03/18.
 */

@Entity(nameInDb = "SALES", indexes = {@Index(value = "mSaleKey", unique = true)})
public class Sale implements Serializable {

    //This field is for Serialization
    private static final long serialVersionUID = 536871008;
    @SerializedName("sale_key")
    @Id
    @Unique
    @Property(nameInDb = "SALE_KEY")
    private String mSaleKey;
    @SerializedName("name")
    @Property(nameInDb = "NAME")
    private String mName;
    @SerializedName("sale")
    @Property(nameInDb = "SALE")
    private String mSale;
    @SerializedName("store")
    @Property(nameInDb = "STORE")
    private String mStore;
    @SerializedName("sale_url")
    @Property(nameInDb = "URL")
    private String mSaleUrl;
    @SerializedName("begins")
    @Property(nameInDb = "BEGINS")
    private String mBegins;
    @SerializedName("ends")
    @Property(nameInDb = "ENDS")
    private String mEnds;
    @SerializedName("description")
    @Property(nameInDb = "DESCRIPTION")
    private String mDescription;
    @SerializedName("size")
    @Property(nameInDb = "SIZE")
    private int mSize;
    @Property(nameInDb = "IMG_COVER_URL")
    private String imageCoverUrl;
    @Property(nameInDb = "IMG_DETAIL_URL")
    private String imageDetailUrl;

    @Generated(hash = 1899620902)
    public Sale(String mSaleKey, String mName, String mSale, String mStore,
                String mSaleUrl, String mBegins, String mEnds, String mDescription,
                int mSize, String imageCoverUrl, String imageDetailUrl) {
        this.mSaleKey = mSaleKey;
        this.mName = mName;
        this.mSale = mSale;
        this.mStore = mStore;
        this.mSaleUrl = mSaleUrl;
        this.mBegins = mBegins;
        this.mEnds = mEnds;
        this.mDescription = mDescription;
        this.mSize = mSize;
        this.imageCoverUrl = imageCoverUrl;
        this.imageDetailUrl = imageDetailUrl;
    }

    @Generated(hash = 983461670)
    public Sale() {
    }

    public String getSaleKey() {
        return mSaleKey;
    }

    public void setSaleKey(String mSaleKey) {
        this.mSaleKey = mSaleKey;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getSaleUrl() {
        return mSaleUrl;
    }

    public void setSaleUrl(String mSaleUrl) {
        this.mSaleUrl = mSaleUrl;
    }

    public String getStore() {
        return mStore;
    }

    public void setStore(String mStore) {
        this.mStore = mStore;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getSale() {
        return mSale;
    }

    public void setSale(String mSale) {
        this.mSale = mSale;
    }

    public String getBegins() {
        return mBegins;
    }

    public void setBegins(String mBegins) {
        this.mBegins = mBegins;
    }

    public String getEnds() {
        return mEnds;
    }

    public void setEnds(String mEnds) {
        this.mEnds = mEnds;
    }

    public String getImageCoverUrl() {
        return imageCoverUrl;
    }

    public void setImageCoverUrl(String imageCoverUrl) {
        this.imageCoverUrl = imageCoverUrl;
    }

    public String getImageDetailUrl() {
        return imageDetailUrl;
    }

    public void setImageDetailUrl(String imageDetailUrl) {
        this.imageDetailUrl = imageDetailUrl;
    }

    @Override
    public String toString() {
        return "Sale{" +
                "saleKey='" + mSaleKey + '\'' +
                '}';
    }

    public String getMSaleKey() {
        return this.mSaleKey;
    }

    public void setMSaleKey(String mSaleKey) {
        this.mSaleKey = mSaleKey;
    }

    public String getMName() {
        return this.mName;
    }

    public void setMName(String mName) {
        this.mName = mName;
    }

    public String getMSale() {
        return this.mSale;
    }

    public void setMSale(String mSale) {
        this.mSale = mSale;
    }

    public String getMStore() {
        return this.mStore;
    }

    public void setMStore(String mStore) {
        this.mStore = mStore;
    }

    public String getMSaleUrl() {
        return this.mSaleUrl;
    }

    public void setMSaleUrl(String mSaleUrl) {
        this.mSaleUrl = mSaleUrl;
    }

    public String getMBegins() {
        return this.mBegins;
    }

    public void setMBegins(String mBegins) {
        this.mBegins = mBegins;
    }

    public String getMEnds() {
        return this.mEnds;
    }

    public void setMEnds(String mEnds) {
        this.mEnds = mEnds;
    }

    public String getMDescription() {
        return this.mDescription;
    }

    public void setMDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public int getMSize() {
        return this.mSize;
    }

    public void setMSize(int mSize) {
        this.mSize = mSize;
    }
}