package com.hakim.bzc.di.component;

import com.hakim.bzc.app.BZCApplication;
import com.hakim.bzc.di.module.AppModule;
import com.hakim.bzc.di.module.DataModule;
import com.hakim.bzc.di.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by fakher on 12/03/18.
 */

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, DataModule.class})
public interface AppComponent {

    void inject(BZCApplication application);
}
