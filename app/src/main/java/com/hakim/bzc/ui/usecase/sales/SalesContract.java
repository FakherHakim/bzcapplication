package com.hakim.bzc.ui.usecase.sales;

import com.hakim.bzc.data.model.entity.Sale;
import com.hakim.bzc.ui.BasePresenter;
import com.hakim.bzc.ui.BaseView;

import java.util.List;

/**
 * Created by fakher on 13/03/18.
 */

public interface SalesContract {

    interface View extends BaseView<Presenter> {

        void setLoadingIndicatorVisible(boolean active);

        void showSales(List<Sale> sales);

        void showSaleDetailsUi(Sale sale);

        void showLoadingSalesError(int httpExceptionCode);

        void showNoSales();
    }

    interface Presenter extends BasePresenter {

        void loadSales(boolean forceUpdate);

        void openSaleDetail(Sale sale);
    }
}
