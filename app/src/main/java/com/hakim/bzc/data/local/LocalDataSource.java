package com.hakim.bzc.data.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.hakim.bzc.data.IDataSource;
import com.hakim.bzc.data.model.entity.DaoMaster;
import com.hakim.bzc.data.model.entity.DaoSession;
import com.hakim.bzc.data.model.entity.Sale;
import com.hakim.bzc.data.model.entity.SaleDao;

import org.greenrobot.greendao.async.AsyncSession;

import java.util.List;

import io.reactivex.BackpressureOverflowStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by fakher on 12/03/18.
 */

public class LocalDataSource implements IDataSource {


    private final AsyncSession mAsyncSession;
    private SaleDao mSaleDao;

    public LocalDataSource(Context context) {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, "sales-db", null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        mAsyncSession = daoSession.startAsyncSession();
        mSaleDao = daoSession.getSaleDao();
        Timber.d("Creating local data source");
    }

    @Override
    public Flowable<List<Sale>> getAllSales() {
        Timber.d("Returning all sales from database");
        return Flowable.just(mSaleDao.loadAll())
                .onBackpressureBuffer(Long.MAX_VALUE,
                        //FIXME add dropped item to list and emits it at the end of the flow.
                        () -> Timber.w("Old items will be dropped"),
                        BackpressureOverflowStrategy.DROP_OLDEST);
    }

    @Override
    public Observable<Sale> getSaleDetail(@NonNull String saleKey) {
        return null;
    }

    @Override
    public void refreshSales() {
        //Nothing to do
    }

    @Override
    public void saveSale(Sale sale) {

        Timber.d("Saving sale in database");
        mAsyncSession.setListener(operation -> Single.fromCallable(operation::isCompletedSucessfully)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(operationSuccess -> {
                    if (operationSuccess) {
                        Timber.i("Sale saved successfully");
                    } else {
                        Timber.e("Sale could not be saved.");
                    }
                }));
        mAsyncSession.insert(sale);
    }
}
