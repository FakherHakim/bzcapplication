package com.hakim.bzc.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import timber.log.Timber;

/**
 * Created by fakher on 18/03/18.
 */

public class NetworStateUtil extends BroadcastReceiver {

    public static final String NETWORK_STATE_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    private INetworkStatusListener mListener;

    public NetworStateUtil(INetworkStatusListener listener) {
        this.mListener = listener;
    }

    public static boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        //should check null because in airplane mode it will be null
        return netInfo != null && netInfo.isConnected();
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(NETWORK_STATE_ACTION)) {

            boolean isConnected = NetworStateUtil.this.isConnected(context);

            if (isConnected) {
                Timber.d("Connection state changed to ON");
                mListener.onConnect();
            } else {
                Timber.d("Connection state changed to OFF");
                mListener.onDisconnect();
            }
        }
    }

    public interface INetworkStatusListener {

        void onConnect();

        void onDisconnect();
    }
}
