package com.hakim.bzc.ui.usecase.sales.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.hakim.bzc.R;

/**
 * Created by fakher on 15/03/18.
 */

public class SalesAdapterListener {

    private final RecyclerView mRecyclerView;
    private OnItemClickListener mOnItemClickListener;
    private OnItemLongClickListener mOnItemLongClickListener;
    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                RecyclerView.ViewHolder holder = mRecyclerView.getChildViewHolder(v);
                mOnItemClickListener.onItemClicked(mRecyclerView, holder.getAdapterPosition(), v);
            }
        }
    };
    private View.OnLongClickListener mOnLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            if (mOnItemLongClickListener != null) {
                RecyclerView.ViewHolder holder = mRecyclerView.getChildViewHolder(v);
                return mOnItemLongClickListener.onItemLongClicked(mRecyclerView, holder.getAdapterPosition(), v);
            }
            return false;
        }
    };
    private RecyclerView.OnChildAttachStateChangeListener mAttachListener
            = new RecyclerView.OnChildAttachStateChangeListener() {
        @Override
        public void onChildViewAttachedToWindow(View view) {
            if (mOnItemClickListener != null) {
                view.setOnClickListener(mOnClickListener);
            }
            if (mOnItemLongClickListener != null) {
                view.setOnLongClickListener(mOnLongClickListener);
            }
        }

        @Override
        public void onChildViewDetachedFromWindow(View view) {

        }
    };

    private SalesAdapterListener(RecyclerView recyclerView) {
        mRecyclerView = recyclerView;
        mRecyclerView.setTag(R.id.sales_fragment_recycler_view, this);
        mRecyclerView.addOnChildAttachStateChangeListener(mAttachListener);
    }

    public static SalesAdapterListener addTo(RecyclerView view) {
        SalesAdapterListener support = (SalesAdapterListener) view.getTag(R.id.sales_fragment_recycler_view);
        if (support == null) {
            support = new SalesAdapterListener(view);
        }
        return support;
    }

    public static SalesAdapterListener removeFrom(RecyclerView view) {
        SalesAdapterListener support = (SalesAdapterListener) view.getTag(R.id.sales_fragment_recycler_view);
        if (support != null) {
            support.detach(view);
        }
        return support;
    }

    public SalesAdapterListener setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
        return this;
    }

    public SalesAdapterListener setOnItemLongClickListener(OnItemLongClickListener listener) {
        mOnItemLongClickListener = listener;
        return this;
    }

    private void detach(RecyclerView view) {
        view.removeOnChildAttachStateChangeListener(mAttachListener);
        view.setTag(R.id.sales_fragment_recycler_view, null);
    }

    public interface OnItemClickListener {

        void onItemClicked(RecyclerView recyclerView, int position, View v);
    }

    public interface OnItemLongClickListener {

        boolean onItemLongClicked(RecyclerView recyclerView, int position, View v);
    }
}
