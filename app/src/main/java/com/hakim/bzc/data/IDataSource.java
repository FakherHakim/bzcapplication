package com.hakim.bzc.data;

import android.support.annotation.NonNull;

import com.hakim.bzc.data.model.entity.Sale;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;

/**
 * Created by fakher on 12/03/18.
 */

public interface IDataSource {

    Flowable<List<Sale>> getAllSales();

    Observable<Sale> getSaleDetail(@NonNull String saleKey);

    void refreshSales();

    void saveSale(Sale sale);
}
