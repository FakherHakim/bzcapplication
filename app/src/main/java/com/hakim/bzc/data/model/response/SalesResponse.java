package com.hakim.bzc.data.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by fakher on 13/03/18.
 */

public class SalesResponse {

    @SerializedName("sales")
    private List<Sale> sales;

    public List<Sale> getSales() {
        return sales;
    }

    @Override
    public String toString() {
        return sales.get(0).toString();
    }

    public class Sale {

        @SerializedName("sale_key")
        private String mSaleKey;

        @SerializedName("name")
        private String mName;

        @SerializedName("sale")
        private String mSale;

        @SerializedName("store")
        private String mStore;

        @SerializedName("sale_url")
        private String mSaleUrl;

        @SerializedName("begins")
        private String mBegins;

        @SerializedName("ends")
        private String mEnds;

        @SerializedName("description")
        private String mDescription;

        @SerializedName("size")
        private int mSize;

        @SerializedName("image_urls")
        private ImageUrls mImageUrls;

        public String getSaleKey() {
            return mSaleKey;
        }

        public void setSaleKey(String mSaleKey) {
            this.mSaleKey = mSaleKey;
        }

        public String getName() {
            return mName;
        }

        public void setName(String mName) {
            this.mName = mName;
        }

        public String getSale() {
            return mSale;
        }

        public void setSale(String mSale) {
            this.mSale = mSale;
        }

        public String getStore() {
            return mStore;
        }

        public void setStore(String mStore) {
            this.mStore = mStore;
        }

        public String getSaleUrl() {
            return mSaleUrl;
        }

        public void setSaleUrl(String mSaleUrl) {
            this.mSaleUrl = mSaleUrl;
        }

        public String getBegins() {
            return mBegins;
        }

        public void setBegins(String mBegins) {
            this.mBegins = mBegins;
        }

        public String getEnds() {
            return mEnds;
        }

        public void setEnds(String mEnds) {
            this.mEnds = mEnds;
        }

        public String getDescription() {
            return mDescription;
        }

        public void setDescription(String mDescription) {
            this.mDescription = mDescription;
        }

        public int getSize() {
            return mSize;
        }

        public void setSize(int mSize) {
            this.mSize = mSize;
        }

        public ImageUrls getImageUrls() {
            return mImageUrls;
        }

        public void setImageUrls(ImageUrls mImageUrls) {
            this.mImageUrls = mImageUrls;
        }
    }

    public class ImageUrls {

        @SerializedName("80x85")
        List<ImageInfo> mImageCover;

        @SerializedName("300x280")
        List<ImageInfo> mImageDetail;

        public List<ImageInfo> getImageCover() {
            return mImageCover;
        }

        public void setImageCover(List<ImageInfo> mImageCover) {
            this.mImageCover = mImageCover;
        }

        public List<ImageInfo> getImageDetail() {
            return mImageDetail;
        }

        public void setImageDetail(List<ImageInfo> mImageDetail) {
            this.mImageDetail = mImageDetail;
        }
    }

    public class ImageInfo {

        private String saleKey;

        @SerializedName("height")
        private int height;

        @SerializedName("width")
        private int width;

        @SerializedName("url")
        private String url;

        public String getSaleKey() {
            return saleKey;
        }

        public void setSaleKey(String saleKey) {
            this.saleKey = saleKey;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
