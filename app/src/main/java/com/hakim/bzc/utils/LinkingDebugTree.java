package com.hakim.bzc.utils;

import android.util.Log;

import timber.log.Timber;

/**
 * Created by fakher on 12/03/18.
 */

public class LinkingDebugTree extends Timber.DebugTree {

    @Override
    protected String createStackElementTag(StackTraceElement element) {
        return super.createStackElementTag(element) + "|" + element.getLineNumber();
    }

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        Log.println(priority, "MyApp", String.format(".(%s.java:%s) - %s", tag.split("\\|")[0], tag.split("\\|")[1], message));
    }
}