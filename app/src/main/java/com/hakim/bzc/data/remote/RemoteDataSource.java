package com.hakim.bzc.data.remote;

import android.support.annotation.NonNull;

import com.hakim.bzc.BuildConfig;
import com.hakim.bzc.data.IDataSource;
import com.hakim.bzc.data.model.entity.Sale;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by fakher on 12/03/18.
 */

public class RemoteDataSource implements IDataSource {

    private ApiService mApiService;

    public RemoteDataSource(ApiService apiService) {
        mApiService = apiService;
        Timber.d("Creating remote data source");
    }

    @Override
    public Flowable<List<Sale>> getAllSales() {

        Timber.d("Get all sales frm remote");
        return mApiService.getSales(BuildConfig.API_KEY)
                .flatMap(salesResponse -> Flowable.fromArray(salesResponse.getSales()))
                .flatMap(Flowable::fromIterable)
                .map(responseSale -> {
                    //For each Sale Response item, generate a Sale entity item.
                    Sale result = new Sale();
                    result.setDescription(responseSale.getDescription());
                    result.setName(responseSale.getName());
                    result.setSaleKey(responseSale.getSaleKey());
                    result.setSale(responseSale.getSale());
                    result.setStore(responseSale.getStore());
                    result.setBegins(responseSale.getBegins());
                    result.setEnds(responseSale.getEnds());
                    result.setImageCoverUrl(responseSale.getImageUrls().getImageCover().get(0).getUrl());
                    result.setImageDetailUrl(responseSale.getImageUrls().getImageDetail().get(0).getUrl());
                    return result;
                })
                .toList()
                .toFlowable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Sale> getSaleDetail(@NonNull String saleKey) {
        return null;
    }

    @Override
    public void refreshSales() {
        //Nothing to do.
    }

    @Override
    public void saveSale(Sale sale) {
        //Nothing to do.
    }
}
