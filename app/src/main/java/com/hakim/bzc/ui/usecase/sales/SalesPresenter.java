package com.hakim.bzc.ui.usecase.sales;

import android.support.annotation.NonNull;

import com.hakim.bzc.data.DataSource;
import com.hakim.bzc.data.model.entity.Sale;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import timber.log.Timber;

/**
 * Created by fakher on 13/03/18.
 */

public class SalesPresenter implements SalesContract.Presenter {

    private SalesContract.View mSalesView;
    private DataSource mDataSource;

    @NonNull
    private CompositeDisposable mCompositeDisposable;

    public SalesPresenter(DataSource dataSource, SalesContract.View view) {
        mDataSource = dataSource;
        mSalesView = view;
        mSalesView.setPresenter(this);
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void subscribe() {
        loadSales(false);
    }

    @Override
    public void unsubscribe() {

        mCompositeDisposable.clear();
    }

    @Override
    public void loadSales(boolean forceUpdate) {
        mSalesView.setLoadingIndicatorVisible(true);
        if (forceUpdate) {
            mDataSource.refreshSales();
        }

        Disposable disposable = mDataSource
                .getAllSales()
                .flatMap(Flowable::fromIterable)
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        // onNext
                        sales -> {
                            processSales(sales);
                            mSalesView.setLoadingIndicatorVisible(false);
                        },
                        // onError
                        throwable -> {
                            mSalesView.setLoadingIndicatorVisible(false);
                            if (throwable instanceof HttpException) {
                                Timber.e(throwable, "Could not get sales");
                                HttpException httpException = (HttpException) throwable;
                                mSalesView.showLoadingSalesError(httpException.code());
                            }
                        });

        mCompositeDisposable.add(disposable);
    }

    @Override
    public void openSaleDetail(Sale sale) {
        mSalesView.showSaleDetailsUi(sale);
    }

    private void processSales(List<Sale> sales) {

        if (sales.isEmpty()) {
            mSalesView.showNoSales();
        } else {
            mSalesView.showSales(sales);
        }
    }
}
