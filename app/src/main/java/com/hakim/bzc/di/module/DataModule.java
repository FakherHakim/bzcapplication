package com.hakim.bzc.di.module;

import android.content.Context;

import com.hakim.bzc.data.DataSource;
import com.hakim.bzc.data.local.LocalDataSource;
import com.hakim.bzc.data.remote.ApiService;
import com.hakim.bzc.data.remote.RemoteDataSource;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by fakher on 12/03/18.
 */

@Module
@Singleton
public class DataModule {

    @Inject
    @Provides
    public RemoteDataSource providesRemoteDataSource(ApiService apiService) {
        return new RemoteDataSource(apiService);
    }

    @Inject
    @Provides
    public LocalDataSource providesLocalDataSource(Context context) {
        return new LocalDataSource(context);
    }

    @Inject
    @Provides
    public DataSource providesDataSource(LocalDataSource local, RemoteDataSource remote) {
        return new DataSource(local, remote);
    }
}
