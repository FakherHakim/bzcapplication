package com.hakim.bzc.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;

import com.hakim.bzc.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by fakher on 15/03/18.
 */

public class UtilMethods {

    public static void addFragmentToActivity(@NonNull FragmentManager fragmentManager,
                                             @NonNull Fragment fragment, int frameId) {

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(frameId, fragment);
        transaction.commit();
    }

    public static String calculateDateDifference(Context context,
                                                 String endDateString,
                                                 String beginDateString) {

        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(tz);

        Date endDate;
        Date beginDate;
        long diffDays = 0;
        try {
            endDate = df.parse(endDateString);
            beginDate = df.parse(beginDateString);
            long diff = Math.abs(endDate.getTime() - beginDate.getTime());
            diffDays = diff / (24 * 60 * 60 * 1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String day = diffDays > 1 ?
                context.getString(R.string.days) : context.getString(R.string.day);

        return diffDays + " " + day;
    }

    public static int getScreenOrientation(Context context) {
        return context.getResources().getConfiguration().orientation;
    }

    public static void showAlertDialog(Context context,
                                       int title,
                                       int message,
                                       DialogInterface.OnClickListener okListener) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, okListener)
                .setIcon(android.R.drawable.ic_dialog_alert);

        builder.show();
    }
}
