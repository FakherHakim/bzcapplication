###This is a technical test project for company BazarChic.###

1-Setup the project:
Before running this project, you must add file properties.gradle in your app/ directory
the template of this file is in properties-sample.gradle. This file contain the end point URL
and the api key used. This file contain sensitive data that must not be visible in repository.

2-Project architecture:
The project use clean architecture and following MVP pattern.