package com.hakim.bzc.app;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.hakim.bzc.BuildConfig;
import com.hakim.bzc.data.DataSource;
import com.hakim.bzc.di.component.AppComponent;
import com.hakim.bzc.di.component.DaggerAppComponent;
import com.hakim.bzc.di.module.AppModule;
import com.hakim.bzc.di.module.DataModule;
import com.hakim.bzc.di.module.NetworkModule;
import com.hakim.bzc.utils.LinkingDebugTree;
import com.squareup.leakcanary.LeakCanary;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by fakher on 12/03/18.
 */

public class BZCApplication extends Application {

    @Inject
    DataSource mDataSource;

    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        //Init Leak Canary.
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);

        if (BuildConfig.DEBUG) {
            Timber.plant(new LinkingDebugTree());
            Stetho.initializeWithDefaults(this);
            Timber.d("App launched. timber log and stetho enabled !");
        }

        //Init App Component.
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .dataModule(new DataModule())
                .build();

        mAppComponent.inject(this);
    }

    public DataSource getDataSource() {
        return mDataSource;
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}
