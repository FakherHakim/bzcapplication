package com.hakim.bzc.ui;

/**
 * Created by fakher on 13/03/18.
 */

public interface BaseView<T> {

    void setPresenter(T presenter);
}
