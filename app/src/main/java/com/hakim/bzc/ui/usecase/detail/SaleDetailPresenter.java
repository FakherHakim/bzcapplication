package com.hakim.bzc.ui.usecase.detail;

import android.support.annotation.NonNull;

import com.hakim.bzc.data.DataSource;
import com.hakim.bzc.data.model.entity.Sale;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by fakher on 13/03/18.
 */

public class SaleDetailPresenter implements SaleDetailContract.Presenter {

    private SaleDetailContract.View mDetailSaleView;
    private DataSource mDataSource;
    private Sale mSale;

    @NonNull
    private CompositeDisposable mCompositeDisposable;

    public SaleDetailPresenter(DataSource dataSource, SaleDetailContract.View view, Sale sale) {

        mDataSource = dataSource;
        mDetailSaleView = view;
        mSale = sale;
        mDetailSaleView.setPresenter(this);
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void subscribe() {

        mDetailSaleView.showDetail(mSale);
        mDetailSaleView.setLoadingIndicatorVisible(true);
    }

    @Override
    public void unsubscribe() {
        mCompositeDisposable.clear();
    }
}
