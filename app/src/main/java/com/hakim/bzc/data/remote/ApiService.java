package com.hakim.bzc.data.remote;

import com.hakim.bzc.data.model.response.SalesResponse;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by fakher on 12/03/18.
 */

public interface ApiService {

    @GET("v1/sales/active.json/")
    Flowable<SalesResponse> getSales(@Query("apikey") String apiKey);
}
