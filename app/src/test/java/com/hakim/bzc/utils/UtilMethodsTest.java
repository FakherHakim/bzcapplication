package com.hakim.bzc.utils;

import android.app.Activity;

import com.hakim.bzc.BuildConfig;
import com.hakim.bzc.ui.usecase.sales.SalesActivity;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

/**
 * Created by fakher on 18/03/18.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class UtilMethodsTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void calculateDateDifference() throws Exception {

        String beginDate = "2018-03-16T12:00:00Z";
        String endDate = "2018-03-19T12:00:00Z";

        String beginDate2 = "2018-03-18T12:00:00Z";
        String endDate2 = "2018-03-19T12:00:00Z";

        Assert.assertEquals("3 Days",
                UtilMethods.calculateDateDifference(RuntimeEnvironment.application,
                        endDate, beginDate));

        Assert.assertEquals("1 Day",
                UtilMethods.calculateDateDifference(RuntimeEnvironment.application,
                        endDate2, beginDate2));

    }

    @Test
    @Config(qualifiers = "land")
    public void getScreenOrientationLand() throws Exception {

        Assert.assertEquals(2,
                UtilMethods.getScreenOrientation(RuntimeEnvironment.application));
    }

    @Test
    public void getScreenOrientation() throws Exception {

        Assert.assertEquals(1,
                UtilMethods.getScreenOrientation(RuntimeEnvironment.application));
    }

}