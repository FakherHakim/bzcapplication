package com.hakim.bzc.ui.usecase.sales;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.hakim.bzc.R;
import com.hakim.bzc.ui.BaseActivity;
import com.hakim.bzc.utils.UtilMethods;

/**
 * Created by fakher on 13/03/18.
 */

public class SalesActivity extends BaseActivity {

    private SalesPresenter mPresenter;
    private CoordinatorLayout mSalesView;
    private Snackbar mSnackBarView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales);

        Toolbar toolbar = findViewById(R.id.sales_activity_toolbar);
        setSupportActionBar(toolbar);

        mSalesView = findViewById(R.id.activity_sales_coordinator_layout);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SalesFragment salesFragment =
                (SalesFragment) getSupportFragmentManager().findFragmentById(R.id.sales_fragment_holder);
        if (salesFragment == null) {
            // Create the fragment
            salesFragment = SalesFragment.newInstance();
            UtilMethods.addFragmentToActivity(
                    getSupportFragmentManager(), salesFragment, R.id.sales_fragment_holder);
        }

        mPresenter = new SalesPresenter(getDataSource(), salesFragment);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sales_fragment, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                mPresenter.loadSales(true);
                break;
        }
        return true;
    }

    @Override
    public void onConnect() {
        if (mSnackBarView != null){
            mSnackBarView.dismiss();
        }
    }

    @Override
    public void onDisconnect() {

        //Show snackBar indicating that there's no connection.
        mSnackBarView = Snackbar.make(mSalesView,
                R.string.error_no_connection, Snackbar.LENGTH_INDEFINITE);
        mSnackBarView.show();
    }
}
