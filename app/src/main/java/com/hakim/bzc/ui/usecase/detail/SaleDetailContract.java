package com.hakim.bzc.ui.usecase.detail;

import com.hakim.bzc.data.model.entity.Sale;
import com.hakim.bzc.ui.BasePresenter;
import com.hakim.bzc.ui.BaseView;

/**
 * Created by fakher on 13/03/18.
 */

public interface SaleDetailContract {

    interface View extends BaseView<Presenter> {

        void setLoadingIndicatorVisible(boolean active);

        void showDetail(Sale sale);
    }

    interface Presenter extends BasePresenter {

    }
}
