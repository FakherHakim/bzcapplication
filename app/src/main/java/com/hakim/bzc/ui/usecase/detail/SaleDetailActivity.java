package com.hakim.bzc.ui.usecase.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;

import com.hakim.bzc.R;
import com.hakim.bzc.data.model.entity.Sale;
import com.hakim.bzc.ui.BaseActivity;
import com.hakim.bzc.utils.UtilMethods;

/**
 * Created by fakher on 13/03/18.
 */

public class SaleDetailActivity extends BaseActivity {

    public static final String EXTRA_SALE_DETAIL_ID = "SALE_DETAIL";
    public CoordinatorLayout mSaleDetailView;
    private SaleDetailContract.Presenter mPresenter;
    private Snackbar mSnackBarView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        mSaleDetailView = findViewById(R.id.activity_sale_detail_coordinator_layout);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Sale sale = (Sale) getIntent().getSerializableExtra(EXTRA_SALE_DETAIL_ID);

        SaleDetailFragment SaleDetailFragment =
                (SaleDetailFragment) getSupportFragmentManager().findFragmentById(R.id.sale_detail_fragment_holder);
        if (SaleDetailFragment == null) {
            // Create the fragment
            SaleDetailFragment = SaleDetailFragment.newInstance();
            UtilMethods.addFragmentToActivity(
                    getSupportFragmentManager(), SaleDetailFragment, R.id.sale_detail_fragment_holder);
        }

        mPresenter = new SaleDetailPresenter(getDataSource(), SaleDetailFragment, sale);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onConnect() {
        if (mSnackBarView != null){
            mSnackBarView.dismiss();
        }
    }

    @Override
    public void onDisconnect() {

        //Show snackBar indicating that there's no connection.
        mSnackBarView = Snackbar.make(mSaleDetailView,
                R.string.error_no_connection, Snackbar.LENGTH_INDEFINITE);
        mSnackBarView.show();
    }
}
