package com.hakim.bzc.ui.usecase.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hakim.bzc.R;
import com.hakim.bzc.data.model.entity.Sale;
import com.hakim.bzc.utils.UtilMethods;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import timber.log.Timber;

/**
 * Created by fakher on 13/03/18.
 */

public class SaleDetailFragment extends Fragment implements SaleDetailContract.View {

    private SaleDetailContract.Presenter mPresenter;
    private ProgressBar mProgress;
    private ImageView mImageView;
    private TextView mNameTV;
    private TextView mDescTV;
    private TextView mStoreTV;
    private TextView mDelayTV;

    public static SaleDetailFragment newInstance() {
        return new SaleDetailFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setPresenter(SaleDetailContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);

        mProgress = rootView.findViewById(R.id.loading_detail_progress_bar);
        mImageView = rootView.findViewById(R.id.fragment_detail_sale_image);
        mNameTV = rootView.findViewById(R.id.fragment_detail_sale_name);
        mDescTV = rootView.findViewById(R.id.fragment_detail_sale_description);
        mStoreTV = rootView.findViewById(R.id.fragment_detail_sale_store);
        mDelayTV = rootView.findViewById(R.id.fragment_detail_sale_delay);

        return rootView;
    }

    @Override
    public void setLoadingIndicatorVisible(boolean active) {
        mProgress.setVisibility(active ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showDetail(Sale sale) {

        Picasso.get()
                .load(sale.getImageDetailUrl())
                .fit()
                .into(mImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        setLoadingIndicatorVisible(false);
                    }

                    @Override
                    public void onError(Exception e) {
                        Timber.e(e, "Could not download image");
                        setLoadingIndicatorVisible(false);
                    }
                });
        mNameTV.setText(sale.getName());
        mDescTV.setText(sale.getDescription());
        mStoreTV.setText(sale.getStore());
        mDelayTV.setText(UtilMethods
                .calculateDateDifference(getActivity(), sale.getEnds(), sale.getBegins()));
    }
}
