package com.hakim.bzc.di.module;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.hakim.bzc.BuildConfig;
import com.hakim.bzc.data.remote.ApiService;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by fakhakim on 14/03/2018.
 */

@Module
public class NetworkModule {

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient() {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG) {

//            Add body logging interceptor
            HttpLoggingInterceptor bodyLogging = new HttpLoggingInterceptor();
            bodyLogging.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(bodyLogging);

            //Add Stetho Interceptor.
            builder.addNetworkInterceptor(new StethoInterceptor());
        }

        //Setting connection and read timeout for 5 seconds.
        builder.connectTimeout(5 * 60 * 1000, TimeUnit.MILLISECONDS)
                .readTimeout(5 * 60 * 1000, TimeUnit.MILLISECONDS);

        //Returning OkHttpClient.
        return builder.build();
    }

    /**
     * This method provides a Retrofit instance as singleton instance.
     *
     * @param okHttpClient pass an OkHttpClient to create the Retrofit client.
     * @return Retrofit instance
     */
    @Inject
    @Provides
    @Singleton
    public Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.client(okHttpClient)
                .baseUrl(BuildConfig.END_POINT)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create());
        return builder.build();
    }

    /**
     * Provides an api service to make calls to the api.
     *
     * @param restAdapter an instance of Retrofit to make the service.
     * @return retrofit api service.
     */
    @Provides
    @Singleton
    public ApiService provideApiService(Retrofit restAdapter) {

        return restAdapter.create(ApiService.class);
    }
}
