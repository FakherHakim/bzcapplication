package com.hakim.bzc.ui;

import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.hakim.bzc.app.BZCApplication;
import com.hakim.bzc.data.DataSource;
import com.hakim.bzc.utils.NetworStateUtil;

/**
 * Created by fakhakim on 15/03/2018.
 */

public abstract class BaseActivity extends AppCompatActivity implements NetworStateUtil.INetworkStatusListener {

    private BZCApplication mApplication;

    private DataSource mDataSource;
    private NetworStateUtil mNetworkStatusReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mApplication = (BZCApplication) getApplication();
        mDataSource = mApplication.getDataSource();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Register to connection state broadcast receiver.
        mNetworkStatusReceiver = new NetworStateUtil(this);
        registerReceiver(mNetworkStatusReceiver,
                new IntentFilter(NetworStateUtil.NETWORK_STATE_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();

        //Unregister to connection state broadcast receiver.
        unregisterReceiver(mNetworkStatusReceiver);
    }

    public DataSource getDataSource() {
        return mDataSource;
    }
}
