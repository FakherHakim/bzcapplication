package com.hakim.bzc.data;

import android.support.annotation.NonNull;

import com.hakim.bzc.data.local.LocalDataSource;
import com.hakim.bzc.data.model.entity.Sale;
import com.hakim.bzc.data.remote.RemoteDataSource;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.BackpressureOverflowStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import timber.log.Timber;

/**
 * Created by fakher on 12/03/18.
 */

public class DataSource implements IDataSource {

    private RemoteDataSource mRemoteDataSource;
    private LocalDataSource mLocalDataSource;

    private Map<String, Sale> mCachedSales;
    private boolean mIsCacheDirty;

    @Inject
    public DataSource(LocalDataSource local, RemoteDataSource remote) {
        mRemoteDataSource = remote;
        mLocalDataSource = local;
        Timber.d("Creating data source");
    }

    @Override
    public Flowable<List<Sale>> getAllSales() {
        // Respond immediately with cache if available and not dirty
        if (mCachedSales != null && !mIsCacheDirty) {
            Timber.d("Returning all sales from cache.");
            return Flowable.fromIterable(mCachedSales.values()).toList().toFlowable();
        } else if (mCachedSales == null) {
            mCachedSales = new LinkedHashMap<>();
        }

        Flowable<List<Sale>> remoteSales = getAndSaveRemoteSales();

        if (mIsCacheDirty) {
            return remoteSales;
        } else {
            // Query the local storage if available. If not, query the network.
            Flowable<List<Sale>> localSales = getAndCacheLocalSales();
            return Flowable.concat(localSales, remoteSales)
                    .filter(sales -> !sales.isEmpty())
                    .firstOrError()
                    .toFlowable();
        }
    }

    @Override
    public Observable<Sale> getSaleDetail(@NonNull String saleKey) {
        return null;
    }

    @Override
    public void refreshSales() {
        mIsCacheDirty = true;
    }

    @Override
    public void saveSale(Sale sale) {

        mLocalDataSource.saveSale(sale);
    }

    private Flowable<List<Sale>> getAndCacheLocalSales() {
        return mLocalDataSource.getAllSales()
                .flatMap(tasks -> Flowable.fromIterable(tasks)
                        .doOnNext(sale -> mCachedSales.put(sale.getSaleKey(), sale))
                        .toList()
                        .toFlowable());
    }

    private Flowable<List<Sale>> getAndSaveRemoteSales() {

        return mRemoteDataSource
                .getAllSales()
                .flatMap(sales -> Flowable.fromIterable(sales)
                        .onBackpressureBuffer(Long.MAX_VALUE,
                                //BackPressure action
                                () -> {
                                    Timber.w("Flowable backPressure, starting to drop oldest.");
                                }, BackpressureOverflowStrategy.DROP_OLDEST)
                        .doOnNext(sale -> {
                            saveSale(sale);
                            mCachedSales.put(sale.getSaleKey(), sale);
                        }).toList().toFlowable())
                //Setting cache dirty to false?
                .doOnComplete(() -> mIsCacheDirty = false);
    }
}
