package com.hakim.bzc.ui.usecase.sales.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hakim.bzc.R;
import com.hakim.bzc.data.model.entity.Sale;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import timber.log.Timber;

/**
 * Created by fakhakim on 15/03/2018.
 */

public class SalesListAdapter extends RecyclerView.Adapter<SalesListAdapter.ViewHolder> {

    private List<Sale> mSaleList;

    public SalesListAdapter(List<Sale> sales) {
        mSaleList = sales;
    }

    @Override
    public SalesListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View saleView = inflater.inflate(R.layout.item_sale_rv, parent, false);

        return new ViewHolder(saleView);
    }

    @Override
    public void onBindViewHolder(SalesListAdapter.ViewHolder holder, int position) {

        Sale sale = mSaleList.get(position);

        //Set view title
        TextView title = holder.titleView;
        title.setText(sale.getName());

        //Set image
        ImageView imageView = holder.coverImageView;
        Picasso.get().load(sale.getImageCoverUrl())
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        Timber.i("image downloaded successfully");
                    }

                    @Override
                    public void onError(Exception e) {
                        Timber.e(e, "image download failed");
                    }
                });
    }

    @Override
    public int getItemCount() {
        return mSaleList.size();
    }

    public List<Sale> getItems() {
        return mSaleList;
    }

    public void setItems(List<Sale> sales) {
        mSaleList = sales;
        notifyDataSetChanged();
    }

    /**
     * Basic view holder class to be used in the recycler view.
     */
    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView titleView;
        ImageView coverImageView;

        ViewHolder(View itemView) {

            super(itemView);
            titleView = itemView.findViewById(R.id.list_item_title_text_view);
            coverImageView = itemView.findViewById(R.id.list_item_image_cover);
        }
    }
}
